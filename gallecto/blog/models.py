import os
from django.db import models
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.safestring import mark_safe

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)
# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length = 250)
    slug = models.SlugField(max_length = 250, null = True, blank = True)
    #thumbnails to MEDIA_ROOT//blog/thumbnails
    thumbnail = models.ImageField(null=True, upload_to="blog/thumbnails")
    shortDescription = models.CharField(max_length=100)
    description = models.TextField(default='')
    index = models.IntegerField(default=0)
    status = models.IntegerField(choices=STATUS,default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now= True)

    class Meta:
        ordering = ['-index']
        verbose_name_plural = "Categories"
    
    def __str__(self):
        return self.title
    
    def thumbnail_tag(self):
        return mark_safe('<img src="{}" width="250" height="160" />'.format(url(self.thumbnail)))

class Post(models.Model):
    category = models.ForeignKey('Category', null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length = 250, null = True, blank = True)
    #Banners to MEDIA_ROOT/blog/banners
    banner = models.ImageField(upload_to="blog/banners")
    description = models.TextField(default='')
    content = RichTextUploadingField(blank =True, null = True)
    status = models.IntegerField(choices=STATUS,default=0)
    published_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now= True)

    class Meta:
        ordering = ['-published_on']
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.title
    
    def banner_tag(self):
        return mark_safe('<img src="{}" width="750" height="300" />'.format(url(self.banner)))

class Comment(models.Model):
    post = models.ForeignKey('Post', null=True, on_delete=models.CASCADE, related_name='comments')
    name = models.CharField(max_length = 80)
    email = models.CharField(max_length = 250)
    text = models.TextField()
    created_on = models.DateTimeField(auto_now_add = True)
    approved = models.BooleanField(default = False)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete = models.CASCADE)

    # def approve_comment(self):
    #     self.approved = True
    
    def __str__(self):
        return self.name

def url(image_path):
    return os.path.join(settings.MEDIA_URL, str(image_path))